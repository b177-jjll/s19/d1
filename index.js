// console.log('Hello World');

// Exponent Operator - is added to simplify the calculation for the exponent of a given number

let firstNum = Math.pow(8,2);

console.log(firstNum);

// ES6 Update

let secondNum = 8 ** 2;

console.log(secondNum);

// Template Literals - allows developer to write without using the concatenation symbol(+)

let name = 'John';

console.log('Message without template literals:')
console.log('Hello ' + name + '! Welcome to programming!');

// Uses backticks(``). Variables are placed inside a placeholde(${}).

let message = `Hello ${name}! Welcome to programming`;

console.log(`Message with template literals:
${message}`);

// Multi-line using Template Literals

let anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.`

console.log(anotherMessage);

let intRate = .1;
	principal = 1000;

console.log(`The interest on your savings account is: ${principal * intRate}`);

// [SECTION] Array Destructuring - allows us to unpack elements in arrays into distinct variables. Allows developer to name array elements with variables instead of using index number.

let fullName = ['Juan', 'Tolits', 'Dela Cruz'];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's good to see you again.`);

// Using Array Destructuring

let [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you again.`);

// [SECTION] Object Destructuring

let person = {
	givenName: 'Jane',
	maidenName: 'Miles',
	familyName: 'Doe'
}

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring - this should be exact property name of the object.

let {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName({givenName, maidenName, familyName}) {
	console.log(`This is printed inside a function:`);
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// Arrow Function

/* Syntax
	let/const functionName = ()  =>{
	statement/s;
	}
*/

// function hello() {
// 	console.log(`Hello World!`);
// }

// hello();

const hello = () => {
	console.log(`Hello World!`);
}

hello();

// const getFullName = ({givenName, maidenName, familyName}) => {
// 	console.log(`This is printed inside a function:`);
// 	console.log(`${givenName} ${maidenName} ${familyName}`);
// }

// getFullName(person);

// Arrow Function with loops

const students = ['John', 'Jane', 'Judy'];

students.forEach(function(student){console.log(`${student} is a student.`)});

students.forEach((student) => {console.log(`${student} is a student.`)});

// Implicit Return Stament

const add = (x,y) => x + y;

let total = add(1,2);

console.log(total);

// Default Function Argument Value

const greet = (name = 'User') => {return `Good evening, ${name}!`};

console.log(greet('John'));
console.log(greet());

// Class-Based Object Blueprint

// Creating a class

class Car{
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object

const myCar = new Car();

console.log(myCar);

myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car('Toyota', 'Vios', 2021);

console.log(myNewCar);